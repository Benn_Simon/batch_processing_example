package spring_batch.Configuration;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import spring_batch.Listener.JobCompletionNotificationListener;
import spring_batch.Model.TransportRequest;
import spring_batch.Processor.TransportRequestItemProcessor;
import spring_batch.ItemWriter.TransportItemWriter;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    TransportItemWriter transportItemWriter;

    @Bean
    public FlatFileItemReader<TransportRequest> reader() {
        return new FlatFileItemReaderBuilder<TransportRequest>()
                .name("TransportRequestReader")
                .resource(new ClassPathResource("sample-data.csv"))
                .delimited()
                .names(TransportRequest.names()).linesToSkip(1)
                .fieldSetMapper(new BeanWrapperFieldSetMapper<TransportRequest>() {{
                    setTargetType(TransportRequest.class);
                }})
                .build();
    }

    @Bean
    public TransportRequestItemProcessor processor() {
        return new TransportRequestItemProcessor();
    }

    @Bean
    public Job handleBulkTransportRequestJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("handleBulkTransportRequestJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<TransportRequest, TransportRequest> chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(transportItemWriter)
                .build();
    }
    // end::jobstep[]
}