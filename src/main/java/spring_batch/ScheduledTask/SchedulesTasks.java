package spring_batch.ScheduledTask;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulesTasks {
    @Autowired
    JobLauncher jobLauncher;
    @Autowired
    Job job;
    @Scheduled(fixedRate = 300000)
    public void run(){

    }

}
