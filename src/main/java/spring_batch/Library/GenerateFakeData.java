package spring_batch.Library;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.abstraction.MockUnit;
import net.andreinc.mockneat.types.enums.DictType;
import org.springframework.stereotype.Component;
import spring_batch.Model.TransportRequest;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

@Component
public class GenerateFakeData {

    static MockNeat InitMock() {
        return MockNeat.threadLocal();
    }

    public static void main(String[] args){
        generateFakeTransportRequestCsv();
    }


    public TransportRequest generateFakeTransportRequest() {
        MockNeat m = InitMock();
        //long userid, String item, String destination, String origin, double weight, double price, String createdon
        MockUnit<TransportRequest> transportRequestGenerator =
                m.constructor(TransportRequest.class).params(
                        1,
                        m.dicts().type(DictType.EN_NOUN_4SYLL).noSpecialChars(),
                        m.cities().us(),
                        m.cities().us(),
                        m.doubles().range(1.0, 1000.0),
                        0.0,
                        m.localDates().toUtilDate().mapToString()
                );
        System.out.println(transportRequestGenerator.val().toString());
        return transportRequestGenerator.val();
    }

    public static void generateFakeTransportRequestCsv() {
        MockNeat m = InitMock();
        String headers = "userid,item,destination,origin,weight,price,createdon";
        final Path path = Paths.get("./src/main/resources/sample-data.csv");
        m.fmt("#{userid},#{item},#{destination},#{origin},#{weight},#{price},#{createdon}")
                .param("userid", m.ints().range(0, 1))
                .param("item", m.dicts().type(DictType.EN_NOUN_4SYLL).noSpecialChars())
                .param("destination", m.dicts().type(DictType.COUNTRY_NAME).noSpecialChars().escapeHtml())
                .param("origin",m.dicts().type(DictType.COUNTRY_NAME).noSpecialChars().escapeHtml())
                .param("weight", m.ints().range(1, 1000))
                .param("price", m.ints().range(0, 1))
                .param("createdon", m.localDates().toUtilDate().mapToString())
                .list(100)
                .consume(list -> {
                    list.add(0,headers);
                    list.forEach(s -> System.out.println(s));
                    try {
                        Files.write(path, list, CREATE, WRITE);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                });

    }

}
