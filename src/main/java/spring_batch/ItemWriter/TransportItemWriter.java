package spring_batch.ItemWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring_batch.Listener.JobCompletionNotificationListener;
import spring_batch.Model.TransportRequest;
import spring_batch.Repository.TransportRequestRepository;

import java.util.List;

@Component
public class TransportItemWriter implements ItemWriter<TransportRequest> {

    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    @Autowired
    TransportRequestRepository transportRequestRepository;

    @Override
    public void write(List<? extends TransportRequest> items) throws Exception {
        log.info("Saved the object ".concat(items.toString()));
        for(TransportRequest transportRequest:items){
                transportRequestRepository.save(transportRequest);
        }
    }
}
