package spring_batch.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring_batch.Model.TransportRequest;

@Repository
public interface TransportRequestRepository extends CrudRepository<TransportRequest, Long> {
}
