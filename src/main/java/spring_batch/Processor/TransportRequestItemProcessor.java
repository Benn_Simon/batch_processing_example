package spring_batch.Processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import spring_batch.Listener.JobCompletionNotificationListener;
import spring_batch.Model.TransportRequest;

import java.util.Date;

public class TransportRequestItemProcessor implements ItemProcessor<TransportRequest,TransportRequest> {
    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    @Override
    public TransportRequest process(TransportRequest item) throws Exception {
        item.setCreatedon(new Date().toLocaleString());
        item.setUserid(2);
        item.setPrice(calculatePrice(item.getDestination(),item.getOrigin(),item.getWeight()));
        log.info("After Processing "+ item.toString());
        return item;
    }

    private Double calculatePrice(String destination, String origin, double weight){
        //suppose 1kg  = 2bob and compareTo gives accurate distance in Km then 1km = 10b0b
        // (in real life you would use Haversian formula)
        double weight_factor = (weight*2);
        double distance_factor = Math.abs(origin.compareTo(destination) * 10);
        return  weight_factor + distance_factor ;
    }
}
